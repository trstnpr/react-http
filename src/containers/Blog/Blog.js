import React, { Component, Suspense } from 'react';
// import axios from 'axios';
import { Route, NavLink, Switch, Redirect } from 'react-router-dom';

import './Blog.css';
import Posts from './Posts/Posts';

// old way of lazyloading component
// import asyncComponent from './../../hoc/asyncComponent';
// const AsyncNewPost = asyncComponent(() => {
//     return import('./NewPost/NewPost');
// });

// v16.6 way of lazyloading component
const AsyncNewPost = React.lazy(() => import('./NewPost/NewPost'));

class Blog extends Component {
    state = {
        auth: true
    }

    render () {
        
        return (
            <div className="Blog">
                <header>
                    <nav>
                        <ul>
                        <li>
                                <NavLink to="/" exact>Home</NavLink>
                            </li>
                            <li>
                                <NavLink
                                    to="/posts/"
                                    exact
                                    activeClassName="my-active"
                                    activeStyle={{
                                        color: 'orange',
                                        textDecoration: 'underline'
                                    }}
                                >Post</NavLink>
                            </li>
                            <li>
                                <NavLink to={{
                                    pathname: '/new-post',
                                    hash: '#submit',
                                    search: '?quick-submit=true'
                            }}>New Post</NavLink>
                            </li>
                        </ul>
                    </nav>
                </header>
                {/* <Route path="/" exact render={() => <h1>Home</h1>} />
                <Route path="/" render={() => <h1>Home 2</h1>} /> */}
                
                {/* rder based on priority */}
                {/* Switch loads the first route that matches the given path. */}
                <Switch>
                    {this.state.auth ? (
                        // old way of lazyloading component
                        // <Route path="/new-post" component={AsyncNewPost} />
                        
                        // v16.6 way of lazyloading component
                        <Route
                            path="/new-post"
                            render={() => (
                                <Suspense fallback={<div>Loading...</div>}>
                                    <AsyncNewPost />
                                </Suspense>
                            )}
                        />
                    ) : null}
                    <Route path="/posts" component={Posts} />
                    {/* <Redirect from="/" to="/posts" /> */}
                    <Route render={() => <h1>404</h1>} /> {/* wont work together with redirect / */}
                    
                </Switch>
            </div>
        );
    }
}

export default Blog;