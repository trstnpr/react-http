import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import axios from 'axios';

// axios global values such auth headers & api keys

// Axios default configs
// base Endpoint
axios.defaults.baseURL = 'https://jsonplaceholder.typicode.com';
axios.defaults.headers.common['Authorization'] = 'AUTH TOKEN/API KEY';

// set header for specific request type (default: applicatoion/json)
axios.defaults.headers.post['Content-Type'] = 'application/json';

// global request handling
axios.interceptors.request.use(request => {
    console.log('request', request);
    // Edit request config
    return request;
}, error => {
    console.log('request error', error);
    return Promise.reject(error); // to still forward it
});

// global response handling
axios.interceptors.response.use(response => {
    console.log('response', response);
    // Edit request config
    return response;
}, error => {
    console.log('response error', error);
    return Promise.reject(error); // to still forward it
});

ReactDOM.render( <App />, document.getElementById( 'root' ) );
registerServiceWorker();
